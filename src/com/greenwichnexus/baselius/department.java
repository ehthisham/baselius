package com.greenwichnexus.baselius;

import java.util.ArrayList;

import android.app.ExpandableListActivity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.Toast;
import android.widget.ExpandableListView.OnChildClickListener;



public class department extends ExpandableListActivity implements
OnChildClickListener {

@Override
public void onCreate(Bundle savedInstanceState) {
super.onCreate(savedInstanceState);
final ExpandableListView expandbleLis = getExpandableListView();
expandbleLis.setDividerHeight(10);
expandbleLis.setGroupIndicator(null);
expandbleLis.setClickable(true);




setGroupData();
setChildGroupData();


NewAdapter mNewAdapter = new NewAdapter(groupItem, childItem,null);

mNewAdapter
		.setInflater(
				(LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE),
				this);
getExpandableListView().setAdapter(mNewAdapter);
expandbleLis.setOnChildClickListener(this);

//Group Listener Close other groups on opening other one
expandbleLis.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
  int previousItem = -1;
  @Override
  public void onGroupExpand(int groupPosition) {
      if (groupPosition != previousItem){
          expandbleLis.collapseGroup(previousItem);
          previousItem = groupPosition;
      }
      else{
          expandbleLis.collapseGroup(groupPosition);
          previousItem = -1;
      }
  }
}
);

}


public void setGroupData() {
	groupItem.add("English");
	groupItem.add("Physics");
	groupItem.add("Chemistry");
	groupItem.add("Economics");
	groupItem.add("Commerce/Management");
	groupItem.add("Malayalam");
	groupItem.add("Mathematics");
groupItem.add("Botany");
groupItem.add("Zoology");
groupItem.add("Political Science");
groupItem.add("Physical Education");
groupItem.add("Other departments");
groupItem.add("Commerce-Self Financing");
groupItem.add("Office and Administration");






}
ArrayList<String> groupItem = new ArrayList<String>();

ArrayList<Object> childItem = new ArrayList<Object>();

public void setChildGroupData() {
/**
 * Add Data For English
 */
ArrayList<String> child = new ArrayList<String>();
child.add("English Programme");
child.add("English Faculty");
child.add("English Association");
child.add("English Contact Details");
childItem.add(child);


/**
 * Add Data For physics
 */

child = new ArrayList<String>();
child.add("Physics Programme");
child.add("Physics Faculty");
child.add("Physics Association");
child.add("Physics Contact Details");
childItem.add(child);


/**
 * Add Data For chemistry
 */
child = new ArrayList<String>();
child.add("Chemistry Programme");
child.add("Chemistry Faculty");
child.add("Chemistry Association");
child.add("Chemistry Contact Details");
childItem.add(child);
/**
 * Add Data For Economics
 */
child = new ArrayList<String>();
child.add("Economics Programme");
child.add("Economics Faculty");
child.add("Economics Association");
child.add("Economics Contact Details");
childItem.add(child);

/**
 * Add Data For commerce/manage
 */
child = new ArrayList<String>();
child.add("Commerce/Management Programme");
child.add("Commerce/Management Faculty");
child.add("Commerce/Management Association");
child.add("Commerce/Management Contact Details");
childItem.add(child);

/**
 * Add Data For malayalam
 */
child = new ArrayList<String>();
child.add("Malayalam Programme");
child.add("Malayalam Faculty");
child.add("Malayalam Association");
child.add("Malayalam Contact Details");
childItem.add(child);
/**
 * Add Data For mathematics
 */
child = new ArrayList<String>();
child.add("Mathematics Programme");
child.add("Mathematics Faculty");
child.add("Mathematics Association");
child.add("Mathematics Contact Details");
childItem.add(child);
/**
 * Add Data For Botany
 */
child = new ArrayList<String>();
child.add("Botany Programme");
child.add("Botany Faculty");
child.add("Botany Association");
child.add("Botany Contact Details");
childItem.add(child);
/**
 * Add Data For Zoology
 */
child = new ArrayList<String>();
child.add("Zoology Programme");
child.add("Zoology Faculty");
child.add("Zoology Association");
child.add("Zoology Contact Details");
childItem.add(child);

/**
 * Add Data For political science
 */
child = new ArrayList<String>();
child.add("Political Science Programme");
child.add("Political Science Faculty");
child.add("Political Science Association");
child.add("Political Science Contact Details");
childItem.add(child);
/**
 * Add Data For physical education
 */
child = new ArrayList<String>();
child.add("Physical Education Faculty");
child.add("Physical Education Contact Details");
childItem.add(child);
/**
 * Add Data For Other department 
 */
child = new ArrayList<String>();

child.add("Other departments Faculty");


childItem.add(child);
/**
 * Add Data For commerce self
 */
child = new ArrayList<String>();

child.add("Commerce-Self Financing Programme");
child.add("Commerce-Self Financing Faculty");
child.add("Commerce-Self Financing Association");
child.add("Commerce-Self Financing Contact Details");
childItem.add(child);
/**
 * Add Data For Commerce-Self Financing
 */
/*child = new ArrayList<String>();
child.add("Programme");
child.add("Faculty");
child.add("Association");
child.add("Contact Details");
childItem.add(child);*/
/**
 * Add Data For Other office and administration
 */
child = new ArrayList<String>();



child.add("Office and Administration Contact Details");
childItem.add(child);
}



@Override
        public boolean onChildClick(ExpandableListView parent, View v,
	           int groupPosition, int childPosition, long id) {
      Toast.makeText(department.this, "Clicked On Child",
		Toast.LENGTH_SHORT).show();


return true;
}
@Override
protected void onPause() {
	// TODO Auto-generated method stub
	super.onPause();
	Data.APP_RUNNING=false;
}
@Override
protected void onResume() {
	// TODO Auto-generated method stub
	super.onResume();
	Data.APP_RUNNING=true;
}}



