package com.greenwichnexus.baselius;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ImageView;

public class Zoology  extends Activity{
	
	ImageView zoph,zomail;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.zoology);
		zoph=(ImageView) findViewById(R.id.zoology_phone);
		zoph.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Intent i=new Intent(Intent.ACTION_VIEW);
				i.setData(Uri.parse("tel:+914812565235"));
				startActivity(i);
				
				
			}
		});
		
		zomail=(ImageView) findViewById(R.id.zoology_mail);
		zomail.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(Intent.ACTION_SEND);intent.setType("message/rfc822");
				intent.putExtra(Intent.EXTRA_SUBJECT, "subject");
				intent.putExtra(Intent.EXTRA_EMAIL, new String[]{"baseliuszoologydept@gmail.com"});
				Intent mailer = Intent.createChooser(intent, null);
				startActivity(mailer);
				
			}
		});
		
	}

}
