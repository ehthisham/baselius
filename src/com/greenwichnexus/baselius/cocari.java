package com.greenwichnexus.baselius;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;

public class cocari extends Activity{

	ImageView ncc,nss,sports,arts,extenacty,other;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.cocaricular); 
		ncc=(ImageView) findViewById(R.id.ncc);
		ncc.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Intent i=new Intent(getApplicationContext(),Web.class);
				i.putExtra("id","file:///android_asset/ncc.html");
				startActivity(i);
				
			}
		});
		nss=(ImageView) findViewById(R.id.nss);
		nss.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i=new Intent(getApplicationContext(),Web.class);
				i.putExtra("id","file:///android_asset/nss.html");
				startActivity(i);
				
			}
		});
		sports=(ImageView) findViewById(R.id.sports);
		sports.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				Intent i=new Intent(getApplicationContext(),Webabout.class);
				i.putExtra("link","sports");
				startActivity(i);
				
			}
		});
		arts=(ImageView) findViewById(R.id.arts_club);
		arts.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i=new Intent(getApplicationContext(),Webabout.class);
				i.putExtra("link","arts");
				startActivity(i);
				
				
			}
		});
		
		extenacty=(ImageView) findViewById(R.id.extension_activities);
		extenacty.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i=new Intent(getApplicationContext(),Webabout.class);
				i.putExtra("link","extensionact");
				startActivity(i);
				
			}
		});
		other=(ImageView) findViewById(R.id.others);
		other.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i=new Intent(getApplicationContext(),Webabout.class);
				i.putExtra("link","otheract");
				startActivity(i);
				
			}
		});
		
		

}	@Override
protected void onPause() {
	// TODO Auto-generated method stub
	super.onPause();
	Data.APP_RUNNING=false;
}
@Override
protected void onResume() {
	// TODO Auto-generated method stub
	super.onResume();
	Data.APP_RUNNING=true;
}}
