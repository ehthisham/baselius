package com.greenwichnexus.baselius;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ImageView;

public class about extends Activity{

	
	ImageView his,vision,misson,aims,objective;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.aboutus); 
		his=(ImageView) findViewById(R.id.history);
		his.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Intent i=new Intent(getApplicationContext(),Web.class);
				i.putExtra("id","file:///android_asset/history.html");
				startActivity(i);
				
			}
		});
		vision=(ImageView) findViewById(R.id.vision);
		vision.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i=new Intent(getApplicationContext(),Web.class);
				i.putExtra("id","file:///android_asset/vision.html");
				startActivity(i);
				
			}
		});
		misson=(ImageView) findViewById(R.id.mission);
		misson.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i=new Intent(getApplicationContext(),Web.class);
				i.putExtra("id","file:///android_asset/mission.html");
				startActivity(i);
			}
		});
		aims=(ImageView) findViewById(R.id.aims);
		aims.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i=new Intent(getApplicationContext(),Web.class);
				i.putExtra("id","file:///android_asset/emblem_and_otto.html");
				startActivity(i);
				
			}
		});
		objective=(ImageView) findViewById(R.id.objective);
		objective.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i=new Intent(getApplicationContext(),Web.class);
				i.putExtra("id","file:///android_asset/objective.html");
				startActivity(i);
			}
		});

}}
