package com.greenwichnexus.baselius;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ImageView;

public class Acadamics extends Activity {
	
	ImageView exam,seminar,research;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.acadamics);
		
		exam=(ImageView) findViewById(R.id.examination_btn);
		exam.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Intent i=new Intent(getApplicationContext(),Webabout.class);
				i.putExtra("link","exam");
				startActivity(i);
				
				
			}
		});
		seminar=(ImageView) findViewById(R.id.seminars_btn);
		seminar.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i=new Intent(getApplicationContext(),Webabout.class);
				i.putExtra("link","seminar");
				startActivity(i);
				
				
			}
		});
		
		research=(ImageView) findViewById(R.id.research_btn);
		research.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i=new Intent(getApplicationContext(),Webabout.class);
				i.putExtra("link","research");
				startActivity(i);
				
				
			}
		});
		
	}

}
