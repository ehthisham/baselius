package com.greenwichnexus.baselius;

public class AddHeading {
	private String head;
	private String body;
	
	public String getHead(){
		return head;
	}
	public String getBody(){
		return body;
	}
	public void setHead(String h){
		head=h;
	}
	public void setBody(String b){
		body=b;
	}
	
}
