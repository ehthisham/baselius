package com.greenwichnexus.baselius;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ImageView;

public class management extends Activity{

	ImageView corporate,education,local,pricipal,admnbody;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.management); 
		corporate=(ImageView) findViewById(R.id.corporate_manager);
		corporate.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Intent i=new Intent(getApplicationContext(),Web.class);
				i.putExtra("id","file:///android_asset/coprative.html");
				startActivity(i);
			}
		});
		education=(ImageView) findViewById(R.id.education_agency);
		education.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Intent i=new Intent(getApplicationContext(),Web.class);
				i.putExtra("id","file:///android_asset/education.html");
				startActivity(i);
			}
		});
		local=(ImageView) findViewById(R.id.local);
		local.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Intent i=new Intent(getApplicationContext(),Web.class);
				i.putExtra("id","file:///android_asset/local.html");
				startActivity(i);
			}
		});
		
		pricipal=(ImageView) findViewById(R.id.principal);
		pricipal.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Intent i=new Intent(getApplicationContext(),PrincipalDesk.class);
			//	i.putExtra("link","principal");
				startActivity(i);
				
				
			}
		});
		
		admnbody=(ImageView) findViewById(R.id.administrative_bodies_btn);
		admnbody.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i=new Intent(getApplicationContext(),Adminbody.class);
				startActivity(i);
				
			}
		});

}	@Override
protected void onPause() {
	// TODO Auto-generated method stub
	super.onPause();
	Data.APP_RUNNING=false;
}
@Override
protected void onResume() {
	// TODO Auto-generated method stub
	super.onResume();
	Data.APP_RUNNING=true;
}}

