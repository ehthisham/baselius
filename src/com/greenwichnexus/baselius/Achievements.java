package com.greenwichnexus.baselius;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.StreamCorruptedException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.Window;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class Achievements extends Activity{

	private String htmlcontent,heading;
	 ListView listview;
	    AchievementAdapter adapter;
	    List<AddHeading> list_ugpg  ;
	    
	   
	    private static final Pattern TAG_REGEX = Pattern.compile("\\*\\*\\*(.+?)\\*\\*\\*",Pattern.DOTALL);

	 //   private static final Pattern TAG_REGEX = Pattern.
	    
	    private static List<String> getTagValues(final String str) {
	        final List<String> tagValues = new ArrayList<String>();
	        final Matcher matcher = TAG_REGEX.matcher(str);
	        while (matcher.find()) {
	        	Log.d("=========match=======", matcher.group(1)+"");
	            tagValues.add(matcher.group(1));
	        }
	        
	        Log.d("GETTAG1", tagValues.toString());
	        Log.d("GETTAG1", Integer.toString(tagValues.size()));
	        return tagValues;
	    }
	    
	    private static final Pattern TAG_REGEX2 = Pattern.compile("###(.+?)###",Pattern.DOTALL);
	    private static List<String> getTagValues2(final String str) {
	        final List<String> tagValues1 = new ArrayList<String>();
	        final Matcher matcher1 = TAG_REGEX2.matcher(str);
	        while (matcher1.find()) {
	        	Log.d("=========match=======", matcher1.group(1)+"");
	            tagValues1.add(matcher1.group(1));
	        }
	        
	        Log.d("GETTAG2", tagValues1.toString());
	        Log.d("GETTAG2", Integer.toString(tagValues1.size()));
	        return tagValues1;
	    }
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.achievements); 
		
		list_ugpg=new ArrayList<AddHeading>();
		
		 ObjectInputStream in;
			try {
				
				
				in = new ObjectInputStream(new FileInputStream(new File(new File(getCacheDir(),"")+"cacheFile.srl")));
				//Log.d(" in   "," "+in); 
				//String jsonObject=(String) in.readObject();
				String jsonArray=(String) in.readObject();
				//JSONObject jsonObject = (JSONObject) in.readObject();
				//Log.d("JSONISHERE", jsonObject.toString());
				// htmlcontent=jsonObject.getString("pdesc");
			        in.close();
			     //   JSONObject j=new JSONObject(jsonObject);
			        JSONArray jarray=new JSONArray(jsonArray);
			    //    Log.d("jarray","jarray  is "+jarray);
			      //  Integer i1=Integer.parseInt(getIntent().getExtras().getString("index").toString());
			       String linkname= getIntent().getExtras().getString("link").toString();
			       
			        for( int k=0;k<jarray.length();k++){
			        	
			        	if((jarray.getJSONObject(k).getString("linkname").toString()).equals(linkname))
			        	{
			        		 
					        JSONObject j=jarray.getJSONObject(k);
					        
					        Log.d("jobject","jobject  is "+j);
					        heading=j.getString("ptitle");
					        htmlcontent=j.getString("pdesc");
					     //   htmlcontent.replaceAll("(&lt;tag&gt;)","tag");
					    //    htmlcontent.replaceAll("(&lt;/tag&gt;)","/tag");
					     //   htmlcontent.replaceAll("(&lt;tag&gt;)","tag1");
					     //   htmlcontent.replaceAll("(&lt;/tag1&gt;)","/tag1");
					      //  htmlcontent.replaceAll("[###]", "<tag1>");
					        Log.e("formated string======", htmlcontent);
					      //  String ab=getIntent().getExtras().getString("heading").toString();
							 TextView txt1 = (TextView)findViewById(R.id.headingabouta);
							 txt1.setText(heading);
							txt1.setTypeface(Typeface.SANS_SERIF);
							 txt1.setTextSize(22);
							 txt1.setTextColor(Color.parseColor("#ffffff"));
							
							 List<String> listhead=getTagValues(htmlcontent);
							 List<String> listbody=getTagValues2(htmlcontent);
							//("===========================================html=========", htmlcontent);
							 //Log.e("=====list1====", listhead.get(0));
							// Log.e("=====list1====", listbody.get(0));
							 System.out.println(Arrays.toString(getTagValues(htmlcontent).toArray()));
							 
							for(int i=0;(i<listhead.size())||(i<listbody.size());i++)
							{
								AddHeading ad=new AddHeading();
								
								if(i<listhead.size()){
								ad.setHead(listhead.get(i).toString());
								}
								if(i<listbody.size())
								{
									ad.setBody(listbody.get(i).toString());
								}
								list_ugpg.add(ad);
							}
					        
							listview = (ListView) findViewById(R.id.listviewa);
				  			 adapter = new AchievementAdapter(Achievements.this, list_ugpg);
				  			 
				  			 listview.setAdapter(adapter); 	        
				      
						      break;
				        
			        	}
			        }
			      //  Log.d("i is  "," i is   "+k);
			      //  if(f==1){
			        	
			        
			        //  txt.setTypeface("Times New Roman");
			       // }
			        
			     //   else 
			      //  {
			        
			      //  }
		      
			
			} catch (StreamCorruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} /*catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}*/ catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			

	}

}

