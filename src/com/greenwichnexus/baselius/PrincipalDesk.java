package com.greenwichnexus.baselius;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Html;
import android.view.Window;
import android.webkit.WebView;
import android.widget.TextView;

public class PrincipalDesk  extends Activity{

	private String htmlcontent,heading;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.principaldesk); 
		TextView txt1=(TextView)findViewById(R.id.headingprincipal);
		txt1.setTypeface(Typeface.SANS_SERIF);
		 txt1.setTextSize(22);
		 txt1.setTextColor(Color.parseColor("#ffffff"));
		 
		
		
		TextView ta=(TextView)findViewById(R.id.addprincipal);
		ta.setText("Prof. Jacob Kurian Onattu\nPrincipal\nBaselius College, Kottayam - 686 001.\nPh: 91-481-2563918 (O), 2463030 (R)\nFax: 91-481-2565958");
		ta.setTextColor(Color.parseColor("#601261"));
		ta.setTypeface(Typeface.SANS_SERIF);
		
		String h="MESSAGE FROM THE PRINCIPAL";
		TextView th=(TextView)findViewById(R.id.headmessage);
		th.setText(h);
		th.setTextColor(Color.parseColor("#601261"));
		th.setTextSize(22);
		th.setTypeface(Typeface.SANS_SERIF);

	TextView t1=(TextView)findViewById(R.id.message);
	String msg="		Baselius College is now basking in the limelight of its Golden Jubilee celebrations. The flagship college of the Orthodox Church in its headquarter city has attained great fame and name over the five decades of its creditable existence. The launching of the Mobile App is yet another feather on its cap.\n\n"+
			"		In the era of information revolution, youngsters have recourse to several new�fangled methods of communication. Electronic media of all sorts have almost made newspapers, magazines and even textbooks obsolete. WhatsApp, Facebook, Twitter, Skype, Smartphones, Chatrooms, YouTube� and what not ! They are at the fingertips of the younger generation and we the elder generation can only stare vide eyed with wonder when ideas, comments, pictures and smileys come and go faster than fairies and witches !\n\n"+
                            "		Baselius College is now proudly launching its own Mobile Application. True, mobile phones are banned on the campus because of their abuse which has resulted in several undesirable developments. Yet the students have them at home and their parents too use these new gadgets avidly. So it is very easy for the College Administration to pass on urgent information with the help of this new tool. I am particularly happy that we are launching this device during the Golden Jubilee Days.\n\n"+
			"		As you know, I am destined to sign off by 31st May 2015. I should only be too happy, if in any way, we could enrich the college and raise it to greater heights before myself (and you too) move on to fresh woods and pastures new.\n\n"+
                            "\n			Wishing you a Blessed 2015,\n";
	t1.setText(msg);
	t1.setTextColor(Color.parseColor("#601261"));
	t1.setTypeface(Typeface.SANS_SERIF);
	TextView td=(TextView)findViewById(R.id.dateplace);
	td.setText("Baselius College,\n29/12/2014");
	td.setTextColor(Color.parseColor("#601261"));
	td.setTypeface(Typeface.SANS_SERIF);
	
	TextView tp=(TextView)findViewById(R.id.principalname);
	tp.setText("Prof. Jacob Kurian	Onattu\nPrincipal");
	tp.setTextColor(Color.parseColor("#601261"));
	tp.setTypeface(Typeface.SANS_SERIF);
	
	}

}