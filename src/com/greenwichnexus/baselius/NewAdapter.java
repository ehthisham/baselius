package com.greenwichnexus.baselius;

import java.util.ArrayList;

import android.R.integer;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.CheckedTextView;
import android.widget.TextView;
import android.widget.Toast;

@SuppressWarnings("unchecked")
public class NewAdapter extends BaseExpandableListAdapter {
	 int previousItem = -1;

	public ArrayList<String> groupItem, tempChild;
	public ArrayList<Object> Childtem = new ArrayList<Object>();
	public LayoutInflater minflater;
	public Activity activity;
	private Context context;

	public NewAdapter(ArrayList<String> grList, ArrayList<Object> childItem, Context context ) {
		groupItem = grList;
		this.Childtem = childItem;
		this.context = context; 
	}
	

	public void setInflater(LayoutInflater mInflater, Activity act) {
		this.minflater = mInflater;
		activity = act;
	}

	@Override
	public Object getChild(int groupPosition, int childPosition) {
		return null;
	}

	@Override
	public long getChildId(final int groupPosition, int childPosition) {
		return 0;
	}

	@Override
	public View getChildView(final int groupPosition, final int childPosition,
			boolean isLastChild, View convertView, ViewGroup parent) {
		tempChild = (ArrayList<String>) Childtem.get(groupPosition);
		
		TextView text = null;
		if (convertView == null) {
			convertView = minflater.inflate(R.layout.childrow, null);
		}
		text = (TextView) convertView.findViewById(R.id.textView1);
		text.setText(tempChild.get(childPosition));
		
		
		convertView.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				
				/*Toast.makeText(activity, tempChild.get(childPosition),
						Toast.LENGTH_SHORT).show();*/
				
				String x=tempChild.get(childPosition);
				
				String y= groupItem.get(groupPosition);
			
				
				
			
				//coding maduppa!!!!!!!!!!!!!!
				 
				
				
				
				if(y.equals("English")&& x.equals("English Programme"))
				{
					
				// loading url to a list view*****
					
					Intent i = new Intent(v.getContext(), Web.class);
					i.putExtra("id","file:///android_asset/english_program.html");
                    v.getContext().startActivity(i);
                    
                    
                    
                 //ending
                   
                    //intent to a java class***** 
                    
					//v.getContext().startActivity(new Intent(v.getContext(),First.class));
					
    				
                    
				}
				
				
			
				if(y.equals("English")&&x.equals("English Faculty"))
					
				{
					
					Intent i = new Intent(v.getContext(), Web.class);
					i.putExtra("id","file:///android_asset/engfaculty.html");
                    v.getContext().startActivity(i);
                   
                    
                    
                    
					/*v.getContext().startActivity(new Intent(v.getContext(),management.class));*/
				}
				
				 if(y.equals("English") && x.equals("English Association"))
				{
					
					Intent i = new Intent(v.getContext(), Web.class);
					i.putExtra("id","file:///android_asset/english_asso.html");
                    v.getContext().startActivity(i);
                    
                    
					/*v.getContext().startActivity(new Intent(v.getContext(),management.class));*/
				}
			 if(y.equals("English") && x.equals("English Contact Details") )
				{
					v.getContext().startActivity(new Intent(v.getContext(),Engcontact.class));
					
					
				}
				
				
			 if(y.equals("Physics")&& x.equals("Physics Programme"))
				{
					
					Intent i = new Intent(v.getContext(), Web.class);
					i.putExtra("id","file:///android_asset/physics_program.html");
                    v.getContext().startActivity(i);
                    
         
					/*v.getContext().startActivity(new Intent(v.getContext(),management.class));*/
				}
				
				
			 if( y.equals("Physics")&&x.equals("Physics Faculty"))
				{
					
					
					Intent i = new Intent(v.getContext(), Web.class);
					i.putExtra("id","file:///android_asset/physics_faculty.html");
                    v.getContext().startActivity(i);
                   
					/*v.getContext().startActivity(new Intent(v.getContext(),management.class));*/
				}
				
			 if(y.equals("Physics") && x.equals("Physics Association"))
				{
					
					Intent i = new Intent(v.getContext(), Web.class);
					i.putExtra("id","file:///android_asset/physics_asso.html");
                    v.getContext().startActivity(i);
                    
					/*v.getContext().startActivity(new Intent(v.getContext(),management.class));*/
				}
				 if(y.equals("Physics") && x.equals("Physics Contact Details"))
				{
					v.getContext().startActivity(new Intent(v.getContext(),Phycntct.class));
				}
				 if( y.equals("Chemistry")&&x.equals("Chemistry Programme"))
				{
					Intent i = new Intent(v.getContext(), Web.class);
					i.putExtra("id","file:///android_asset/chemistry_program.html");
                    v.getContext().startActivity(i);
                  
					/*v.getContext().startActivity(new Intent(v.getContext(),management.class));*/
				}
				 if(y.equals("Chemistry")&&x.equals("Chemistry Faculty") )
				{
					Intent i = new Intent(v.getContext(), Web.class);
					i.putExtra("id","file:///android_asset/chemistry_faculty.html");
                    v.getContext().startActivity(i);
                   
					/*v.getContext().startActivity(new Intent(v.getContext(),management.class));*/
				}
			 if(y.equals("Chemistry")&&x.equals("Chemistry Association"))
				{
					
					Intent i = new Intent(v.getContext(), Web.class);
					i.putExtra("id","file:///android_asset/chemistry_association.html");
                    v.getContext().startActivity(i);
					/*v.getContext().startActivity(new Intent(v.getContext(),management.class));*/
				}
			 if( y.equals("Chemistry")&&x.equals("Chemistry Contact Details"))
				{
					v.getContext().startActivity(new Intent(v.getContext(),Chemiscntct.class));
				}
				 if( y.equals("Economics")&&x.equals("Economics Programme"))
				{
					Intent i = new Intent(v.getContext(), Web.class);
					i.putExtra("id","file:///android_asset/economics_program.html");
                    v.getContext().startActivity(i);
					/*v.getContext().startActivity(new Intent(v.getContext(),management.class));*/
				}
			 if(y.equals("Economics")&&x.equals("Economics Faculty"))
				{
					Intent i = new Intent(v.getContext(), Web.class);
					i.putExtra("id","file:///android_asset/economic_faculty.html");
                    v.getContext().startActivity(i);
					/*v.getContext().startActivity(new Intent(v.getContext(),management.class));*/
				}
				 if( y.equals("Economics")&&x.equals("Economics Association"))
				{
					
					Intent i = new Intent(v.getContext(), Web.class);
					i.putExtra("id","file:///android_asset/economics_asso.html");
                    v.getContext().startActivity(i);
					/*v.getContext().startActivity(new Intent(v.getContext(),management.class));*/
				}
			 if( y.equals("Economics")&&x.equals("Economics Contact Details"))
				{
					v.getContext().startActivity(new Intent(v.getContext(),Economics.class));
				}
				 if( y.equals("Commerce/Management")&&x.equals("Commerce/Management Programme"))
				{
					Intent i = new Intent(v.getContext(), Web.class);
					i.putExtra("id","file:///android_asset/commersemanage_program.html");
                    v.getContext().startActivity(i);
					/*v.getContext().startActivity(new Intent(v.getContext(),management.class));*/
				}
				 if( y.equals("Commerce/Management")&&x.equals("Commerce/Management Faculty"))
				{
					Intent i = new Intent(v.getContext(), Web.class);
					i.putExtra("id","file:///android_asset/commercemanage_faculty.html");
                    v.getContext().startActivity(i);
					/*v.getContext().startActivity(new Intent(v.getContext(),management.class));*/
				}
				 if( y.equals("Commerce/Management")&&x.equals("Commerce/Management Association"))
				{
					
					Intent i = new Intent(v.getContext(), Web.class);
					i.putExtra("id","file:///android_asset/commerce_aided.html");
                    v.getContext().startActivity(i);/*v.getContext().startActivity(new Intent(v.getContext(),management.class));*/
				}
				 if( y.equals("Commerce/Management")&&x.equals("Commerce/Management Contact Details"))
				{
					v.getContext().startActivity(new Intent(v.getContext(),Comermanage.class));
				}
				
				 if(y.equals("Malayalam") && x.equals("Malayalam Programme"))
				{
					Intent i = new Intent(v.getContext(), Web.class);
					i.putExtra("id","file:///android_asset/malayalam_program.html");
                    v.getContext().startActivity(i);
					/*v.getContext().startActivity(new Intent(v.getContext(),management.class));*/
				}
				 if(y.equals("Malayalam")&& x.equals("Malayalam Faculty"))
				{
					Intent i = new Intent(v.getContext(), Web.class);
					i.putExtra("id","file:///android_asset/malayalam_faculty.html");
                    v.getContext().startActivity(i);
					/*v.getContext().startActivity(new Intent(v.getContext(),management.class));*/
				}
				 if( y.equals("Malayalam")&&x.equals("Malayalam Association"))
				{
					
					Intent i = new Intent(v.getContext(), Web.class);
					i.putExtra("id","file:///android_asset/malayalam_asso.html");
                    v.getContext().startActivity(i);
					/*v.getContext().startActivity(new Intent(v.getContext(),management.class));*/
				}
				 if(y.equals("Malayalam")&&x.equals("Malayalam Contact Details"))
				{
					v.getContext().startActivity(new Intent(v.getContext(),Malayalam.class));
				}
				 if(y.equals("Mathematics")&&x.equals("Mathematics Programme"))
				{
					Intent i = new Intent(v.getContext(), Web.class);
					i.putExtra("id","file:///android_asset/mathematics_program.html");
                    v.getContext().startActivity(i);
					/*v.getContext().startActivity(new Intent(v.getContext(),management.class));*/
				}
				 if( y.equals("Mathematics")&&x.equals("Mathematics Faculty"))
				{
					Intent i = new Intent(v.getContext(), Web.class);
					i.putExtra("id","file:///android_asset/maths_faculty.html");
                    v.getContext().startActivity(i);
					/*v.getContext().startActivity(new Intent(v.getContext(),management.class));*/
				}
				 if(y.equals("Mathematics")&&x.equals("Mathematics Association"))
				{
					
					Intent i = new Intent(v.getContext(), Web.class);
					i.putExtra("id","file:///android_asset/maths_asso.html");
                    v.getContext().startActivity(i);
					/*v.getContext().startActivity(new Intent(v.getContext(),management.class));*/
				}
				 if( y.equals("Mathematics")&&x.equals("Mathematics Contact Details"))
				{
					v.getContext().startActivity(new Intent(v.getContext(),Mathematics.class));
				}
				 if(y.equals("Botany")&&x.equals("Botany Programme"))
				{
					Intent i = new Intent(v.getContext(), Web.class);
					i.putExtra("id","file:///android_asset/botany_program.html");
                    v.getContext().startActivity(i);
					/*v.getContext().startActivity(new Intent(v.getContext(),management.class));*/
				}
				 if(y.equals("Botany")&&x.equals("Botany Faculty"))
				{
					Intent i = new Intent(v.getContext(), Web.class);
					i.putExtra("id","file:///android_asset/botany_faculty.html");
                    v.getContext().startActivity(i);
					/*v.getContext().startActivity(new Intent(v.getContext(),management.class));*/
				}
				 if(y.equals("Botany")&&x.equals("Botany Association"))
				{
					
					Intent i = new Intent(v.getContext(), Web.class);
					i.putExtra("id","file:///android_asset/botany_association.html");
                    v.getContext().startActivity(i);
					/*v.getContext().startActivity(new Intent(v.getContext(),management.class));*/
				}
				 if( y.equals("Botany")&&x.equals("Botany Contact Details"))
				{
					v.getContext().startActivity(new Intent(v.getContext(),Botany.class));
				}
				
				 if( y.equals("Zoology")&&x.equals("Zoology Programme"))
				{
					Intent i = new Intent(v.getContext(), Web.class);
					i.putExtra("id","file:///android_asset/zoology_program.html");
                    v.getContext().startActivity(i);
					/*v.getContext().startActivity(new Intent(v.getContext(),management.class));*/
				}
				
				 if( y.equals("Zoology")&&x.equals("Zoology Faculty"))
				{
					Intent i = new Intent(v.getContext(), Web.class);
					i.putExtra("id","file:///android_asset/zoology_faculty.html");
                    v.getContext().startActivity(i);
					/*v.getContext().startActivity(new Intent(v.getContext(),management.class));*/
				}
				 if( y.equals("Zoology")&&x.equals("Zoology Association"))
				{
					
					Intent i = new Intent(v.getContext(), Web.class);
					i.putExtra("id","file:///android_asset/zoology_asso.html");
                    v.getContext().startActivity(i);
					/*v.getContext().startActivity(new Intent(v.getContext(),management.class));*/
				}
			 if(y.equals("Zoology")&&x.equals("Zoology Contact Details"))
				{
					v.getContext().startActivity(new Intent(v.getContext(),Zoology.class));
				}
				
			 if(y.equals("Political Science")&&x.equals("Political Science Programme"))
				{
					
					Intent i = new Intent(v.getContext(), Web.class);
					i.putExtra("id","file:///android_asset/politicalscience_program.html");
                    v.getContext().startActivity(i);
					/*v.getContext().startActivity(new Intent(v.getContext(),management.class));*/
				}
			 if(y.equals("Political Science")&&x.equals("Political Science Faculty"))
				{
					Intent i = new Intent(v.getContext(), Web.class);
					i.putExtra("id","file:///android_asset/politicalscience_faculty.html");
                    v.getContext().startActivity(i);
					/*v.getContext().startActivity(new Intent(v.getContext(),management.class));*/
				}
				 if(y.equals("Political Science")&&x.equals("Political Science Association"))
				{
					
					Intent i = new Intent(v.getContext(), Web.class);
					i.putExtra("id","file:///android_asset/politicalscience_asso.html");
                    v.getContext().startActivity(i);
					/*v.getContext().startActivity(new Intent(v.getContext(),management.class));*/
				}
				 if(y.equals("Political Science")&&x.equals("Political Science Contact Details"))
				{
					
					v.getContext().startActivity(new Intent(v.getContext(),Political.class));
				}
				
				 if(y.equals("Physical Education")&&x.equals("Physical Education Faculty"))
				{
					Intent i = new Intent(v.getContext(), Web.class);
					i.putExtra("id","file:///android_asset/physicaleducation_faculty.html");
                    v.getContext().startActivity(i);
					/*v.getContext().startActivity(new Intent(v.getContext(),management.class));*/
				}
				 if( y.equals("Physical Education")&&x.equals("Physical Education Contact Details"))
				{
					v.getContext().startActivity(new Intent(v.getContext(),Physicaleducation.class));
				}
				 if(y.equals("Other departments")&&x.equals("Other departments Faculty"))
				{
					Intent i = new Intent(v.getContext(), Web.class);
					i.putExtra("id","file:///android_asset/otherdepartment_faculty.html");
                    v.getContext().startActivity(i);
					/*v.getContext().startActivity(new Intent(v.getContext(),management.class));*/
				}
				
				 if(y.equals("Commerce-Self Financing")&&x.equals("Commerce-Self Financing Programme"))
				{
					Intent i = new Intent(v.getContext(), Web.class);
					i.putExtra("id","file:///android_asset/commerceselffin_program.html");
                    v.getContext().startActivity(i);
					/*v.getContext().startActivity(new Intent(v.getContext(),management.class));*/
				}
				 if(y.equals("Commerce-Self Financing")&&x.equals("Commerce-Self Financing Faculty"))
				{
					Intent i = new Intent(v.getContext(), Web.class);
					i.putExtra("id","file:///android_asset/commerceselfinace_faculty.html");
                    v.getContext().startActivity(i);
					/*v.getContext().startActivity(new Intent(v.getContext(),management.class));*/
				}
				 if( y.equals("Commerce-Self Financing")&&x.equals("Commerce-Self Financing Association"))
				{
					
					Intent i = new Intent(v.getContext(), Web.class);
					i.putExtra("id","file:///android_asset/commerce_self.html");
                    v.getContext().startActivity(i);
					/*v.getContext().startActivity(new Intent(v.getContext(),management.class));*/
				}
				 if(y.equals("Commerce-Self Financing") && x.equals("Commerce-Self Financing Contact Details"))
				{
					v.getContext().startActivity(new Intent(v.getContext(),Commerceself.class));
				}
				
				
				 if(y.equals("Office and Administration")&&x.equals("Office and Administration Contact Details"))
				{
					Intent i = new Intent(v.getContext(), Web.class);
					i.putExtra("id","file:///android_asset/officeadmin.html");
                    v.getContext().startActivity(i);
					
					/*v.getContext().startActivity(new Intent(v.getContext(),Officeadmin.class));*/
				}
				
			}
		
			
			
		});
		return convertView;
	}

	
	@Override
	public int getChildrenCount(int groupPosition) {
		return ((ArrayList<String>) Childtem.get(groupPosition)).size();
	}

	@Override
	public Object getGroup(int groupPosition) {
		return null;
	}

	@Override
	public int getGroupCount() {
		return groupItem.size();
	}

	@Override
	public void onGroupCollapsed(int groupPosition) {
		
		super.onGroupCollapsed(groupPosition);
	}
	
	
	
	

	@Override
	public void onGroupExpanded(int groupPosition) {
		
		
		
		super.onGroupExpanded(groupPosition);
		
	}

	@Override
	public long getGroupId(int groupPosition) {
		return 0;
	}

	@Override
	public View getGroupView(int groupPosition, boolean isExpanded,
			View convertView, ViewGroup parent) {
		if (convertView == null) {
			convertView = minflater.inflate(R.layout.department, null);
		}
		((CheckedTextView) convertView).setText(groupItem.get(groupPosition));
		((CheckedTextView) convertView).setChecked(isExpanded);
		return convertView;
	}

	@Override
	public boolean hasStableIds() {
		return false;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		
		return false;
	}

}
