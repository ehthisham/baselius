package com.greenwichnexus.baselius;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class AchievementAdapter extends BaseAdapter {
	
	// Declare Variables
    Context mContext;
    LayoutInflater inflater;
    private List<AddHeading> list_headbody = null;
    //private List<TripObject> arraylist;
 
    public  AchievementAdapter(Context context,List<AddHeading> list_add){
        
    	mContext = context;
        this.list_headbody = list_add;
        inflater = LayoutInflater.from(mContext);
        this.list_headbody = new ArrayList<AddHeading>();
        this.list_headbody.addAll(list_add);
    }
 
    public class ViewHolder {
    	
    	TextView heada;
        TextView bodya;
       // TextView occupied;
       // TextView timing;
    }
 
    @Override
    public int getCount() {
        return list_headbody.size();
    }
 
    @Override
    public AddHeading getItem(int position) {
        return list_headbody.get(position);
    }
 
    @Override
    public long getItemId(int position) {
        return position;
    }
 
    public View getView(final int position, View view, ViewGroup parent) {
        final ViewHolder holder;
        if (view == null) {
            holder = new ViewHolder();
            view = inflater.inflate(R.layout.itemview1, null);
            // Locate the TextViews in listview_item.xml
            holder.heada = (TextView) view.findViewById(R.id.headpattern);
            holder.bodya = (TextView) view.findViewById(R.id.bodypattern);
            
          //  holder.occupied = (TextView) view.findViewById(R.id.occupied);
           // holder.day = (TextView) view.findViewById(R.id.separator);
            
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        holder.heada.setText(Html.fromHtml(list_headbody.get(position).getHead()));
        
      //  holder.heada.setBackgroundResource(R.drawable.purplebar);
        holder.heada.setTextColor(Color.WHITE);
        holder.heada.setTypeface(Typeface.SANS_SERIF);
       
       
        	holder.bodya.setText(Html.fromHtml(list_headbody.get(position).getBody()));
            holder.bodya.setTextColor(Color.parseColor("#601261"));
            holder.bodya.setTypeface(Typeface.SANS_SERIF);
        
       
       // Set the results into TextViews
  /*      if(list_ugpg.get(position).getLeave()== null || list_ugpg.get(position).getLeave()=="" || list_ugpg.get(position).getLeave()== "null")
        	holder.spe.setText(list_ugpg.get(position).getSpecialistName());
        else{
        	Log.d("LEAVE TAG", list_ugpg.get(position).getLeave());
        	SpannableString text = new SpannableString(list_ugpg.get(position).getSpecialistName()+"( "+list_ugpg.get(position).getLeave()+" )"); 
        	int start = list_ugpg.get(position).getSpecialistName().length() + 3;
        	int end = start+ list_ugpg.get(position).getLeave().length();
        	text.setSpan(new ForegroundColorSpan(Color.RED), start - 1, end, 0);  
        	holder.spe.setText(text, BufferType.SPANNABLE);
        }
        
        
        holder.spe_class.setText(list_ugpg.get(position).getSpecialization());
        holder.timing.setText(list_ugpg.get(position).getTiming());
        holder.day.setText(list_ugpg.get(position).getDay());
        
        if(list_ugpg.get(position).getDay()==null)
        	holder.day.setVisibility(View.GONE);
        else
        	holder.day.setVisibility(View.VISIBLE);
        */
        // Listen for ListView Item Click
        view.setOnClickListener(new OnClickListener() {
  
            @Override
            public void onClick(View arg0) {
            	
            }
        });
        	return view;
    }

}
