package com.greenwichnexus.baselius;


import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ImageView;

public class Adminbody extends Acadamics {
	
	ImageView collegecouncil,iqac,teacherinchag,pta;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.adminbody);
		
		collegecouncil=(ImageView) findViewById(R.id.college_council_btn);
		collegecouncil.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i=new Intent(getApplicationContext(),Web.class);
				i.putExtra("id","file:///android_asset/college_council.html");
				startActivity(i);
				
			}
		});
		
		
		iqac=(ImageView) findViewById(R.id.iqac_btn);
		iqac.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i=new Intent(getApplicationContext(),Web.class);
				i.putExtra("id","file:///android_asset/iqac.html");
				startActivity(i);
				
			}
		});
		teacherinchag=(ImageView) findViewById(R.id.teachers_in_charge_activities);
		teacherinchag.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i=new Intent(getApplicationContext(),Webabout.class);
				i.putExtra("link","teacherinchag");
				startActivity(i);
				
				
			}
		});
		
		pta=(ImageView) findViewById(R.id.pta);
		pta.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i=new Intent(getApplicationContext(),Web.class);
				i.putExtra("id","file:///android_asset/pta.html");
				startActivity(i);
				
			}
		});
		
		
	}

}
