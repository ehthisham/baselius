package com.greenwichnexus.baselius;

public class UgPgItem {
	private String dept,nofseats,occupied;

    public String getdept(){
    		return dept;
    }
    
    public void setdept(String p_dept){
		this.dept = p_dept;
    }
    
    public String getnofseats(){
		return nofseats;
    }
    
    public void setnofseats(String p_nofseats){
		this.nofseats = p_nofseats; 
    }
    
    public String getoccupied(){
		return occupied;
    }
    
    public void setoccupied(String p_occupied){
		this.occupied = p_occupied; 
    }
    

}
