package com.greenwichnexus.baselius;

import com.greenwichnexus.baselius.R.id;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ImageView;

public class placement extends Activity{
	ImageView plaoff,placactivity,placnews;

	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.placement); 
		plaoff=(ImageView) findViewById(R.id.placement_officer);
		plaoff.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				
				Intent i=new Intent(getApplicationContext(),Web.class);
				i.putExtra("id","file:///android_asset/placement_agency.html");
				startActivity(i);
				
			}
		});
		
		placactivity=(ImageView) findViewById(R.id.activities);
		placactivity.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				
				Intent i=new Intent(getApplicationContext(),Webabout.class);
				i.putExtra("link","placactivity");
				startActivity(i);
				
				
			}
		});
		placnews=(ImageView) findViewById(R.id.placement_news);
		placnews.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				
				Intent i=new Intent(getApplicationContext(),Webabout.class);
				i.putExtra("link","placnews");
				startActivity(i);
				
				
			}
		});
		

}	@Override
protected void onPause() {
	// TODO Auto-generated method stub
	super.onPause();
	Data.APP_RUNNING=false;
}
@Override
protected void onResume() {
	// TODO Auto-generated method stub
	super.onResume();
	Data.APP_RUNNING=true;
}}
