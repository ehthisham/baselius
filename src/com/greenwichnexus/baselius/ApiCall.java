package com.greenwichnexus.baselius;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.DefaultHttpClient;


import android.util.Log;
 
 public class ApiCall {
 
    static String response = null;
    public final static int GET = 1;
    public final static int POST = 2;
 
    public ApiCall() {
    	
    }
 
    public String makeServiceCall(String url, int method) {
        return this.makeServiceCall(url, method, null,false);
    }
  
    public String makeServiceCall(String p_url, int method,List<NameValuePair> params,boolean isFromCache) {
        
    	try {
            
        	// http client
            DefaultHttpClient httpClient = new DefaultHttpClient();
            StringBuilder jsonResults = new StringBuilder();
                        
             if (method == GET) {
                // appending params to url
            	
                if (params != null) {
                    String paramString = URLEncodedUtils
                            .format(params, "utf-8");
                    p_url += "?" + paramString;
                }
                
              //  Log.d("OUR URL IS ",p_url);
                
                HttpURLConnection conn = null;
                URL url = new URL(p_url);
                conn = (HttpURLConnection) url.openConnection();
                
                if(isFromCache)
                	conn.addRequestProperty("Cache-Control", "max-stale=" + 60 * 60 * 24 * 28);
                else
                	conn.addRequestProperty("Cache-Control", "no-cache");
                
                
        	    conn.setUseCaches(true);
        	    
        	    InputStreamReader in = new InputStreamReader(conn.getInputStream());
    	        
    	        // Load the results into a StringBuilder
    	        int read;
    	        char[] buff = new char[1024];
    	        while ((read = in.read(buff)) != -1) {
    	            	jsonResults.append(buff, 0, read);
    	        }
                
              //  Log.d("OUR TAG", jsonResults.toString());
            }
            
            response = jsonResults.toString();
            
          //  Log.d("DefaultHTTP result String:  ",response);
            
 
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    	 catch (Exception e) {
             e.printStackTrace();
         }
         
        return response;
 
    }
}