package com.greenwichnexus.baselius;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.ImageView;

public class Commerceself  extends Activity{
	
	
	ImageView cmselfphone,cmselfmail;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.commerceself);
		cmselfphone=(ImageView) findViewById(R.id.commerce_self_financing_phone);
		cmselfphone.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Intent i=new Intent(Intent.ACTION_VIEW);
				i.setData(Uri.parse("tel:+914812300222"));
				startActivity(i);
				
				
			}
		});
		cmselfmail=(ImageView) findViewById(R.id.commerce_self_financing_mail);
		cmselfmail.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(Intent.ACTION_SEND);intent.setType("message/rfc822");
				intent.putExtra(Intent.EXTRA_SUBJECT, "subject");
				intent.putExtra(Intent.EXTRA_EMAIL, new String[]{"hod@vbcombaselius.ac.in"});
				Intent mailer = Intent.createChooser(intent, null);
				startActivity(mailer);
				
			}
		});
	}

}
