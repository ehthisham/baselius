package location;

import java.text.DecimalFormat;

public final class Distance {
     double lat1,lat2,lon1,lon2;
	 public Distance(double lat1,double lon1,double lat2, double lon2)
	 {
		 this.lat1=lat1;
		 this.lon1=lon1;
		 this.lat2=lat2;
		 this.lon2=lon2;
	 }
	
	 public double find() 
	 {
		   
	
		    int EARTH_RADIUS_KM = 6371;
		    double lat1Rad = Math.toRadians(lat1);
		    double lat2Rad = Math.toRadians(lat2);
		    double deltaLonRad = Math.toRadians(lon2 - lon1);

		    double dist_travelled = Math
		            .acos(Math.sin(lat1Rad) * Math.sin(lat2Rad) + Math.cos(lat1Rad)
		                    * Math.cos(lat2Rad) * Math.cos(deltaLonRad))
		            * EARTH_RADIUS_KM;

		    dist_travelled = Double.parseDouble(new DecimalFormat("##.######")
		            .format(dist_travelled));
		    return Math.round(dist_travelled); 
	 
		
	 }
	 
	
}
